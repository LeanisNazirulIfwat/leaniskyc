import sqlite3


def get_info():
    try:
        db_conn = sqlite3.connect("database/sqllite_kyc.db")
        c = db_conn.cursor()

        upgrade_q = c.execute(
            f'SELECT * FROM users WHERE user_processed = 0')
        return upgrade_q
    except sqlite3.Error as error:
        print("Failed to read data in sqlite table", error)


def get_info_linux():
    try:
        db_conn = sqlite3.connect("/var/www/kyc_mobile/sqllite_kyc.db")
        c = db_conn.cursor()

        upgrade_q = c.execute(
            f'SELECT * FROM users WHERE user_processed = 0')
        return upgrade_q
    except sqlite3.Error as error:
        print("Failed to read data in sqlite table", error)


def insert_data(fullname, address, id_number, sex_type, liveness_percent, user_processed):
    try:
        # sqliteConnection = sqlite3.connect("database/sqllite_kyc.db")
        sqliteConnection = sqlite3.connect("/var/www/kyc_mobile/sqllite_kyc.db")
        cursor = sqliteConnection.cursor()
        print("Successfully Connected to SQLite")

        sqlite_insert_query = f"""INSERT INTO users
                              (ocr_fullname, ocr_address, ocr_idnumber, ocr_sextype, liveness_percent, user_processed) 
                               VALUES 
                              ({fullname},{address},{id_number},{sex_type},{liveness_percent},{user_processed})"""

        count = cursor.execute(sqlite_insert_query)
        sqliteConnection.commit()
        print("Record inserted successfully into Sqllite_kyc table ", cursor.rowcount)
        cursor.close()

    except sqlite3.Error as error:
        print("Failed to insert data into sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("The SQLite connection is closed")


def update_table(identity, fullname, address, id_number, sex_type, liveness_percent, user_processed):
    try:
        # sqliteConnection = sqlite3.connect("database/sqllite_kyc.db")
        sqliteConnection = sqlite3.connect("/var/www/kyc_mobile/sqllite_kyc.db")
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        sql_update_query = f"""Update users set ocr_fullname= ?, ocr_address=?, ocr_idnumber=?, ocr_sextype=?
                  , liveness_percent=?, user_processed=? where id = ?"""
        data = (fullname, address, id_number, sex_type, liveness_percent, user_processed, identity)
        cursor.execute(sql_update_query, data)
        sqliteConnection.commit()
        print("Record Updated successfully ")
        cursor.close()

    except sqlite3.Error as error:
        print("Failed to update sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("The SQLite connection is closed")
