import click
import logic.nric_image_detection as nc
import cv2 as cv
import numpy as np
import shared_utils
from logic.scheduler_job import Scheduler_job
from database import query
import face_recognition as nfc
import schedule
import time
import os
import base64


@click.group()
def cli():
    """
    [Summary]
    Leanis CLI for KYC purpose
    """


@click.group(name='get')
def get_function():
    """
    [Summary]
    CLI For Getting List of Function
    """


@click.command(name='hello_world')
def get_hello_world():
    click.echo("hello World")


@click.command(name='compare_face')
@click.option('--path1', '-p1', help='Path of Image for reference 1', required=True)
@click.option('--path2', '-p2', help='Path of Image for reference 1', required=True)
def get_compare_image(path1, path2):
    """
    [Summary]
    Passing 2 path of image and compare their face
    """

    image_compare_data = nc.calculate_image_detection(path1, path2)
    vis = np.concatenate((shared_utils.fixed_resize_image(image_compare_data.image_a),
                          shared_utils.fixed_resize_image(image_compare_data.image_b)), axis=0)
    cv.imshow('Result', vis)
    # cv.imshow('Image IC', image_compare_data.image_a)
    # cv.imshow('Image Selfie', image_compare_data.image_b)
    print(f'Result: {image_compare_data.result} \n'
          f'Similarity: {image_compare_data.distance}')
    cv.waitKey(0)


get_function.add_command(get_compare_image)
get_function.add_command(get_hello_world)
cli.add_command(get_function)


def schedule_task():
    try:
        Scheduler_job.get_face_from_ic()
    except query:
        print("error to update")
    finally:
        print("Task Running...")


if __name__ == '__main__':
    # result = Scheduler_job.populate_data_linux()
    # for data in result:
    #     print(data['path_front_id'])
    #     front_image = nfc.load_image_file('/var/www/kyc_mobile'+data['path_front_id'])
    #     if front_image is None:
    #         raise Exception("could not load image !")
    #     else:
    #         user_id = data['user_uuid']
    #         print(front_image.shape)
    #         print(f'{user_id} : image loaded')
    schedule.every(1).minutes.do(schedule_task)
    print('Scheduler Job Running.....')
    while 1:
        schedule.run_pending()
        time.sleep(1)
