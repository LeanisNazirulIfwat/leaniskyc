import cv2 as cv
import face_recognition as frc
import shared_utils
from model import image_characteristic as ml, image_validation


def draw_rec_image(image):
    rgb_color_image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
    face_loc = frc.face_locations(rgb_color_image)
    encode_image = frc.face_encodings(rgb_color_image)

    if face_loc is not None:
        for x in face_loc:
            cv.rectangle(rgb_color_image, (x[3], x[0]), (x[1], x[2]), (255, 0, 255), 2)
            print(x)
        data = ml.CharacteristicImage(rgb_color_image, face_loc, encode_image)
    return data


def calculate_image_detection(img_a, img_b):
    img1 = cv.imread(img_a, 0)
    img2 = cv.imread(img_b, 0)
    image_a = draw_rec_image(img1)
    image_b = draw_rec_image(img2)

    encode_image = image_a.face_encode
    encode2_image = image_b.face_encode
    results = frc.compare_faces([encode_image[0]], encode2_image[0], 0.5)
    distance_result = frc.face_distance([encode_image[0]], encode2_image[0])
    print(results, distance_result)

    # resize image
    resized = shared_utils.resize_image(image_a.image, 50)
    resized2 = shared_utils.resize_image(image_b.image, 50)

    cv.putText(resized, f'{results} {round(distance_result[0], 2)}', (50, 50), cv.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255),
               2)
    cv.putText(resized2, f'{results} {round(distance_result[0], 2)}', (50, 50), cv.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255),
               2)
    # pass data validation
    data = image_validation.Validation(resized, resized2, results, distance_result)

    return data
