import os

import shared_utils
from database import query
import shared_utils as su
from logic import nric_image_detection as nric
import face_recognition as nfc
import cv2 as cv
import pytesseract
from pytesseract import Output
import csv

# pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
pytesseract.pytesseract.tesseract_cmd = r'/usr/bin/tesseract'


def populate_data():
    data = query.get_info()
    list_data = su.populate_data_from_db(data.fetchall())
    return list_data
    print(list_data)


def populate_data_linux():
    data = query.get_info_linux()
    list_data = su.populate_data_from_db(data.fetchall())
    return list_data
    print(list_data)


# Python program to get average of a list
def average(lst):
    return sum(lst) / len(lst)


def predict_text(img, x, y, h, w):
    # reading image using opencv

    image = img
    y = y
    x = x
    h = h
    w = w
    crop = image[y:y + h, x:x + w]
    # converting image into gray scale image
    gray_image = cv.cvtColor(crop, cv.COLOR_BGR2GRAY)

    # converting it to binary image by Thresholding
    # this step is require if you have colored image because if you skip this part

    # then tesseract won't able to detect text correctly and this will give incorrect result
    threshold_img = cv.threshold(gray_image, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)[1]

    # configuring parameters for tesseract

    custom_config = r'--oem 3 --psm 6'

    # now feeding image to tesseract

    details = pytesseract.image_to_data(threshold_img, output_type=Output.DICT, config=custom_config, lang='msa')
    print(details['text'])
    total_boxes = len(details['text'])
    print(total_boxes)
    for sequence_number in range(total_boxes):
        if int(float(details['conf'][sequence_number])) > 30:
            (x, y, w, h) = (
                details['left'][sequence_number], details['top'][sequence_number], details['width'][sequence_number],
                details['height'][sequence_number])

            threshold_img = cv.rectangle(threshold_img, (x, y), (x + w, y + h), (0, 255, 0), 2)

    # display image
    # cv.imshow('image_threshold', threshold_img)

    parse_text = []

    word_list = []

    last_word = ''

    for word in details['text']:

        if word != '':
            word_list.append(word)

            last_word = word

    if (last_word != '' and word == '') or (word == details['text'][-1]):
        parse_text.append(word_list)
        word_list = []

    with open('result_text.txt', 'w', newline="") as file:
        csv.writer(file, delimiter=" ").writerows(parse_text)

    # Maintain output window until user presses a key
    # cv.waitKey(0)

    # Destroying present windows on screen
    # cv.destroyAllWindows()
    return ' '.join(parse_text[0])


def get_face_from_ic():
    list_id = []
    query_data = populate_data_linux()
    if query_data:
        for x in query_data:
            front = x['path_front_id']
            back = x['path_back_id']
            video = x['path_video']

            # load front image and video
            # front = "assets\\Image\\Hafiy.jpg"
            # video = "assets\\Video\\HafiyVideo.MOV"
            front_os = os.path.abspath('assets/image/Hafiy.jpg')
            video_os = os.path.abspath('assets/video/HafiyVideo.MOV')
            # front_image = nfc.load_image_file('/var/www/kyc_mobile'+front) -default
            front_image = cv.imread('/var/www/kyc_mobile'+front)
            if front_image is None:
                raise Exception("could not load image !")
            else:
                print(front_image.shape)
            cap = cv.VideoCapture('/var/www/kyc_mobile'+video)

            # finding face location and encoding for front image
            front_image = cv.cvtColor(front_image, cv.COLOR_BGR2RGB)
            front_face_location = nfc.face_locations(front_image)
            front_face_encoding = nfc.face_encodings(front_image)

            if not front_face_encoding:
                print(f"{x['id']} No face Detected")
                continue
            else:
                # finding face location and encoding for video
                video_face_location, video_face_encoding = shared_utils.video_extraction(cap)

                if not video_face_encoding:
                    print(f"{x['id']} No face Detected in the video")
                    continue
                else:
                    # compare face encoding between video and front image
                    similarity_check = []
                    similarity_bool = []
                    for encode in video_face_encoding:
                        kyc_face_distance_result = nfc.face_distance(front_face_encoding[0], encode)
                        kyc_compare_result = nfc.compare_faces(front_face_encoding[0], encode, 0.5)
                        similarity_check.append(kyc_face_distance_result)
                        similarity_bool.append(kyc_compare_result)
                    print(similarity_bool)
                    print(similarity_check)
                    kyc_details = get_kyc_details(front)
                    user_id = x['id']
                    average_kyc = float(average(similarity_check))

                    ic_number, address, fullname, gender = kyc_details
                    query.update_table(identity=user_id, fullname=fullname, address=address, id_number=ic_number,
                                       sex_type=gender,
                                       liveness_percent=average_kyc, user_processed=1)

                    print("Update Success")
    else:
        print("No query data for now")


def get_kyc_details(image_path):
    # get all information point in kyc
    x_ic_number, y_ic_number, w_ic_number, h_ic_number = shared_utils.location_ic_number()
    x_ic_address, y_ic_address, w_ic_address, h_ic_address = shared_utils.location_address()
    x_ic_name, y_ic_name, w_ic_name, h_ic_name = shared_utils.location_name()
    x_ic_gender, y_ic_gender, w_ic_gender, h_ic_gender = shared_utils.location_gender()

    image = image_path
    image = "assets\\Image\\NizamIC.jpeg"
    image_os = os.path.abspath('assets/image/NizamIC.jpeg')
    front_image = cv.imread(image_os)
    if front_image is None:
        raise Exception("could not load image !")
    else:
        front_image.shape

    # predict text from image
    result_ic_number = predict_text(front_image, x_ic_number, y_ic_number, h_ic_number, w_ic_number)
    result_ic_address = predict_text(front_image, x_ic_address, y_ic_address, h_ic_address, w_ic_address)
    result_ic_name = predict_text(front_image, x_ic_name, y_ic_name, h_ic_name, w_ic_name)
    result_ic_gender = predict_text(front_image, x_ic_gender, y_ic_gender, h_ic_gender, w_ic_gender)

    return result_ic_number, result_ic_address, result_ic_name, result_ic_gender
