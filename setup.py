from setuptools import setup, find_packages

setup(
    name='Leanis_KYC Package',
    packages=find_packages(),
    email='nazirul.ifwat@leanis.com.my',
    author='Nazirul Ifwat',
    install_requires=[
        'click==8.0.1',
        'numpy==1.19.5',
        'face-recognition==1.3.0',
        'opencv-python==4.5.3.56',
        'pytesseract==0.3.8',
        'schedule==1.1.0'
    ],
    version='0.0.1',
    entry_points='''
    [console_scripts]
    cli=main:cli
    '''
)
