import cv2 as cv
import numpy as np
import json
import face_recognition as frc


def resize_image(img, scale):
    scale_percent = scale  # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)

    resized = cv.resize(img, dim, interpolation=cv.INTER_AREA)
    return resized


def find_contour(img):
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    invGamma = 1.0 / 0.3
    table = np.array([((i / 255.0) ** invGamma) * 255 for i in np.arange(0, 256)]).astype(
        "uint8"
    )

    # apply gamma correction using the lookup table
    gray = cv.LUT(gray, table)

    ret, thresh1 = cv.threshold(gray, 60, 255, cv.THRESH_BINARY)

    contours, hierarchy = cv.findContours(thresh1, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)[
                          -2:
                          ]
    return contours, hierarchy


def biggestRectangle(contours):
    biggest = None
    max_area = 0
    indexReturn = -1
    for index in range(len(contours)):
        i = contours[index]
        area = cv.contourArea(i)
        if area > 100:
            peri = cv.arcLength(i, True)
            approx = cv.approxPolyDP(i, 0.1 * peri, True)
            if area > max_area:  # and len(approx)==4:
                biggest = approx
                max_area = area
                indexReturn = index
    return indexReturn


def crop_image(img):
    contours, hierarchy = find_contour(img)
    indexReturn = biggestRectangle(contours)
    hull = cv.convexHull(contours[indexReturn])

    # create a crop mask
    mask = np.zeros_like(img)  # Create mask where white is what we want, black otherwise
    cv.drawContours(mask, contours, indexReturn, 255, -1)  # Draw filled contour in mask
    out = np.zeros_like(img)  # Extract out the object and place into output image
    out[mask == 255] = img[mask == 255]

    # crop the image
    (y, x, _) = np.where(mask == 255)
    (topy, topx) = (np.min(y), np.min(x))
    (bottomy, bottomx) = (np.max(y), np.max(x))
    out = img[topy: bottomy + 1, topx: bottomx + 1, :]
    return out


def crop_image_ic(image, coords, saved_location):
    cropped_image = image.crop(coords)
    cropped_image.save(saved_location)
    cropped_image.show()


def crop_image_id(images):
    y = 600
    x = 0
    h = 791
    w = 560
    crop = images[y:y + h, x:x + w]
    return crop


def crop_image_license(images):
    y = 546
    x = 450
    h = 791
    w = 560
    crop = images[y:y + h, x:x + w]
    return crop


def save_keypoint(kp, file_name):
    data = {}
    cnt = 0
    for i in kp:
        data['KeyPoint_%d' % cnt] = []
        data['KeyPoint_%d' % cnt].append({'x': i.pt[0]})
        data['KeyPoint_%d' % cnt].append({'y': i.pt[1]})
        data['KeyPoint_%d' % cnt].append({'size': i.size})
        cnt += 1
    with open(file_name, 'w') as outfile:
        json.dump(data, outfile)


def load_keypoint(kp_location):
    result = []
    with open(kp_location) as json_file:
        data = json.load(json_file)
        cnt = 0
        while data.__contains__('KeyPoint_%d' % cnt):
            pt = cv.KeyPoint(x=data['KeyPoint_%d' % cnt][0]['x'], y=data['KeyPoint_%d' % cnt][1]['y'],
                             _size=data['KeyPoint_%d' % cnt][2]['size'])
            result.append(pt)
            cnt += 1
    return result


def save_descriptor(des, file_name):
    np.savetxt(file_name, des)


def load_descriptor(file_path):
    load_file = np.loadtxt(file_path).astype('float32')
    return load_file


def populate_data_from_db(data):
    user_all_profile = []
    for x in data:
        user_profile = {'id': x[0], 'user_uuid': x[1], 'path_front_id': x[2], 'path_back_id': x[3], 'path_video': x[4],
                        'ocr_fullname': x[5], 'ocr_address': x[6], 'ocr_idnumber': x[7],
                        'ocr_sextype': x[8], 'liveness_percent': x[9], 'user_processed': x[10]}
        user_all_profile.append(user_profile)

    return user_all_profile


def video_extraction(video):
    """"extract 4 frame of video and create face location and encoding based on that"""
    cap = video
    face_location = []
    face_encoding = []
    i = 0
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break
        video_face_location = frc.face_locations(frame)
        video_face_encoding = frc.face_encodings(frame)

        if not video_face_encoding:
            # print('No face Detected in the video')
            continue
        else:
            print('face Detected in the video')
            face_location.append(video_face_location)
            face_encoding.append(video_face_encoding)
            i = i + 1
            if i == 4:
                break

    cap.release()

    print(f'video face location : {len(face_location)}')
    print(f'video face encoding : {len(face_encoding)}')
    return face_location, face_encoding


def location_address():
    y = 600
    x = 0
    h = 791
    w = 560
    return x, y, w, h


def location_name():
    y = 522
    x = 30
    h = 80
    w = 560
    return x, y, w, h


def location_ic_number():
    y = 177
    x = 41
    h = 80
    w = 460
    return x, y, w, h


def location_gender():
    y = 728
    x = 1039
    h = 80
    w = 140
    return x, y, w, h
